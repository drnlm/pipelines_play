FROM ubuntu:18.04
MAINTAINER Neil <neil@somewhere.nowhere.org>

COPY . /opt/src

RUN apt-get update && apt-get -y upgrade
RUN useradd tester
RUN chown -R tester /opt/src
RUN apt-get update && apt-get -y install npm git
