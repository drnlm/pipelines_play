import os

RELEASE = os.getenv("RELEASE")

if not RELEASE:
    print "RELEASE not set in the enviromanet"
else:
    print "RELEASE set to: %s" % RELEASE
